package DTUpay.barcodeservice.rest;


import com.google.zxing.WriterException;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;


/**
 * @author Jonas
 * Barcode generator rest endpoint
 */

@Path("/barcode")
public class BarcodeEndpoint {


	/**
	 * Generates a barcode for the input tokenid. Does not validate if the token is valid. Simply forwards the tokenid to
	 * barcodeManager and returns a png of the barcode
	 * @param tokenid : String (PathParam)
	 * @return Response
	 * @throws IOException
	 * @throws WriterException
	 */
	@GET
	@Path("{tokenid}")
	@Produces("image/png")
	public Response doGet(@PathParam("tokenid") String tokenid) throws IOException, WriterException {
		RestApplication.bm.barcodeImageGen(tokenid);

		File repositoryFile = new File("barcode.png");

        return Response.ok(repositoryFile).build();
	}
}
