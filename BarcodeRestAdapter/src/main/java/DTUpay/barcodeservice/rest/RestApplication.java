package DTUpay.barcodeservice.rest;

import cairo.BarcodeManager;
import cairo.IBarcodeManager;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author Jonas
 * Init BarcodeManager
 */

@ApplicationPath("/")
public class RestApplication extends Application {
    public static IBarcodeManager bm;
    public RestApplication(){
        super();
        bm = new BarcodeManager();
    }
}
