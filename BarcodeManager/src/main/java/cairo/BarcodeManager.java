package cairo;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author Jonas
 * Very simple produces barcode file with google zwing lib.
 * Similar to online example with QRcode
 */

public class BarcodeManager implements IBarcodeManager{

    public void barcodeImageGen(String TokId) throws IOException, WriterException {

        int width = 400;
        int height = 300;
        String imageFormat = "png";

        BitMatrix bitMatrix = new Code128Writer().encode(TokId, BarcodeFormat.CODE_128, width, height);
        MatrixToImageWriter.writeToStream(bitMatrix, imageFormat, new FileOutputStream(new File("barcode.png")));


    }
}
