package cairo;

import com.google.zxing.WriterException;
import java.io.IOException;

/**
 * @author Jonas
 */

public interface IBarcodeManager{
    void barcodeImageGen(String TokId) throws IOException, WriterException;
}
